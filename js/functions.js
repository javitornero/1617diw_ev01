var menuVisible=false;
var banderaScroll=false;
window.addEventListener("load",iniciar,false);
function iniciar(){ 
    var mobMenuRow = document.getElementById("mobMenuRow");
    var contenedor = document.getElementById("main");
    var nav = document.getElementById("nav");
    mobMenuRow.addEventListener("click",accion,false);
    contenedor.addEventListener("click",accionEscondeMenu,false);
    nav.addEventListener("click",accionEscondeMenu,false);
    document.addEventListener("scroll", animaHeader,false);
}

var lastScrollPosition = 0;
var newScrollPosition = 0;
function animaHeader(){
    var newScrollPosition = window.scrollY; 
    if(banderaScroll==true){
        if (newScrollPosition < lastScrollPosition){
            document.getElementById('mobMenuRow').className="mobMenuRowShow";
        }else if(banderaScroll==true){
            document.getElementById('mobMenuRow').className="mobMenuRowHide";
            if(menuVisible==true ){
                esconderBurguer();
            }
        }
    }
    lastScrollPosition = newScrollPosition;
    banderaScroll=true;
}
function accion(){
    if(menuVisible==false){
        sacarBurguer();
    }else{
        esconderBurguer();        
    }
}
function accionEscondeMenu(){
    if(menuVisible==true){
        esconderBurguer();
    }
    banderaScroll=false; // Mantiene el header visible aunque haya scroll
}
function sacarBurguer(){
    document.getElementById("nav").className="sacaMenu";
    menuVisible=true;
    document.getElementById("menuResponsive").className="iconoQuitaMenu";
}
function esconderBurguer(){
    document.getElementById("nav").className="escondeMenu";
    menuVisible=false;
    document.getElementById("menuResponsive").className="iconoSacaMenu";
}