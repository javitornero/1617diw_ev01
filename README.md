Javier Tornero Montellano

DIW

Licencias:

Licensing

We have made these icons available for you to incorporate them into your products under the Apache License Version 2.0. Feel free to remix and re-share these icons and documentation in your products. We'd love attribution in your app's about screen, but it's not required. The only thing we ask is that you not re-sell the icons themselves.

https://icomoon.io/
https://material.io/icons/
License: CC BY 4.0 
https://creativecommons.org/licenses/by/4.0/